package co.simplon.promo16.spotiplon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo16.spotiplon.entity.Album;
import co.simplon.promo16.spotiplon.repository.AlbumRepository;

@Controller
public class HomeController {
    @Autowired
    private AlbumRepository repo;

    @GetMapping("/")
    public String showHomePage(Model model) {
        
        model.addAttribute("albums", repo.findAll());
        return "index";
    }

    @GetMapping("/album/{id}")
    public String showOneAlbum(@PathVariable int id, Model model) {
        Album album = repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        model.addAttribute("album", album);
        return "album";
    }
}
