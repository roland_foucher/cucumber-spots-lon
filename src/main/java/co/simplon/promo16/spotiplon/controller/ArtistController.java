package co.simplon.promo16.spotiplon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo16.spotiplon.entity.Artist;
import co.simplon.promo16.spotiplon.repository.ArtistRepository;

@Controller
public class ArtistController {
    @Autowired
    private ArtistRepository repo;

    @GetMapping("/artist/{id}")
    public String showArtistPage(@PathVariable int id, Model model) {
        
        Artist artist = repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        model.addAttribute("artist", artist);
        return "artist";
    }
}
