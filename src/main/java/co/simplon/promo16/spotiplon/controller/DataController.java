package co.simplon.promo16.spotiplon.controller;

import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import co.simplon.promo16.spotiplon.entity.Album;
import co.simplon.promo16.spotiplon.entity.Artist;
import co.simplon.promo16.spotiplon.entity.Song;

@Controller
public class DataController {
    @Autowired
    private EntityManager em;
    
    @Transactional
    @GetMapping("/generate")
    public String test() {
        

        for (int i = 1; i <= 10; i++) {
            Artist artist = new Artist("Artist "+i, "Pop", "https://cdn.pixabay.com/photo/2016/03/28/09/36/music-1285165_960_720.jpg", LocalDate.of(2021, i, 1));
            em.persist(artist);
            for (int j = 1; j <= 5; j++) {
                Album album = new Album("Artist "+i+" Album "+j, LocalDate.of(2021, i, j), "https://cdn.pixabay.com/photo/2015/06/23/09/13/music-818459_960_720.jpg");
                album.setArtist(artist);
                em.persist(album);
                for (int k = 1; k <= 5; k++) {
                    Song song = new Song("Song "+k, i*j*k, "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3");
                    song.getAlbums().add(album);
                    em.persist(song);
                }
            }
            
        }

        // Artist artist = repo.findById(1);
        // repo.delete(artist);
        // repo.delete(new Artist(2, "name","style", "picture", LocalDate.now()));
        return "test";
    }
}
