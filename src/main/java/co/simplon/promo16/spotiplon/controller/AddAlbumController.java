package co.simplon.promo16.spotiplon.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;

import co.simplon.promo16.spotiplon.entity.Album;
import co.simplon.promo16.spotiplon.repository.AlbumRepository;
import co.simplon.promo16.spotiplon.repository.ArtistRepository;
import co.simplon.promo16.spotiplon.service.Uploader;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class AddAlbumController {
    @Autowired
    private AlbumRepository repoAlbum;
    @Autowired
    private ArtistRepository repoArtist;
    @Autowired
    private Uploader uploader;

    @GetMapping("/add-album")
    public String showForm(Model model) {
        model.addAttribute("album", new Album());
        model.addAttribute("artists", repoArtist.findAll());
        return "add-album";
    }

    @PostMapping("/add-album")
    public String processForm(Album album, @RequestParam("file") MultipartFile file) {
        try {
            album.setCover(uploader.upload(file));
            repoAlbum.save(album);
        } catch (IOException e) {
            
            e.printStackTrace();
        }
        return "redirect:/";
    }

}
