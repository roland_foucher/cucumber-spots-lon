package co.simplon.promo16.spotiplon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.spotiplon.entity.Album;

@Repository
public interface AlbumRepository extends JpaRepository<Album,Integer>{
    
}
