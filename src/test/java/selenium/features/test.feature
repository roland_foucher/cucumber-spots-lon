@Tests
Feature: Ajouter un album

  Scenario: Se rendre sur la page principale
    Given je lance l'explorer et je vais sur l'url de l'application
    When je suis sur la page "Hom"
    Then je vois une grille d'album
    And je vois lalbum "Artist 1 Album 2" dans le tableau

  Scenario: Je vais sur la page add album
    Given je suis sur la page "Home"
    When je clique sur le lien add album
    Then je suis sur la page "Add Album"

  Scenario: J'ajoute un album
    Given je suis sur la page "Add Album"
    When j'ajoute le text "Black Album" dansle champs name
    When j'ajoute la date "10/07/2022" dans le champs date
    When j'ajoute l'artiste "Artist 1" dans le champs artiste
    When je clique sur le bouton "Add"
    Then je vois lalbum "Black Album" dans le tableau
