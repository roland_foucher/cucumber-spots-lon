package selenium.step;

import io.cucumber.java.AfterAll;
import io.cucumber.java.BeforeAll;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import selenium.service.ElementManager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Step {

  private static WebDriver driver = null;

  /**
   * Initialise le chrome driver pour selenium
   */
  @BeforeAll
  public static void init() {
    System.setProperty("webdriver.chrome.driver", "driver/chromedriver");
    driver = new ChromeDriver();
  }

  /**
   * Permet de fermer la page après les tests
   */
  // @AfterAll
  // public static void end() {
  // driver.close();
  // }

  /**
   * On se rend sur l'url de spring boot
   */
  @Given("je lance l'explorer et je vais sur l'url de l'application")
  public void jeLanceLExplorerEtJeVaisSurLUrlDeLApplication() {
    driver.get("http://localhost:8080/");
  }

  /**
   * Vérifie que le titre de la page soit bien celui voulu
   */
  @When("je suis sur la page {string}")
  public void jeSuisSurLaPage(String label) {
    ElementManager.assertElementNotNull(String.format("//h1[normalize-space() = '%s']", label), driver);
  }

  /**
   * Vérifie que la table existe
   */
  @Then("je vois une grille d'album")
  public void jeVoisUneGrilleDAlbum() {
    ElementManager.assertElementNotNull("//table[@class = 'table']", driver);
  }

  /**
   * clique sur le lien add albume pour se rendre sur cette page
   */
  @When("je clique sur le lien add album")
  public void je_clique_sur_le_lien_add_album() {
    ElementManager.cliqueOnElement("//h3//a[@href = '/add-album']", driver);
  }

  /**
   * Verifie que l'album recherché soit dans le tableau
   */
  @When("je vois lalbum {string} dans le tableau")
  public void je_vois_l_album_dans_le_tableau(String label) {
    ElementManager.assertElementNotNull(String.format("//td//a[normalize-space() = '%s']", label), driver);
  }

  /**
   * Ajoute un text dans l'input du formulaire name
   */
  @When("j'ajoute le text {string} dansle champs name")
  public void jAjouteLeTextDansLeChampsName(String label) {
    ElementManager.addTextInFormInput("//input[@name = 'name']", driver, label);
  }

  /**
   * ajoute un artist dans le select du formulaire au champs artist
   */
  @When("j'ajoute l'artiste {string} dans le champs artiste")
  public void j_ajoute_l_artiste_dans_le_champs_artiste(String label) {
    ElementManager.selectInput("//select[@id = 'artist']", driver, label);
  }

  /**
   * Ajoute une date dans le champ date
   */
  @When("j'ajoute la date {string} dans le champs date")
  public void j_ajoute_la_date_dans_le_champs_date(String s) {
    ElementManager.addTextInFormInput("//input[@name = 'released']", driver, "10072022");
  }

  /**
   * clique sur un boutton de la page
   */
  @When("je clique sur le bouton {string}")
  public void je_clique_sur_le_bouton(String s) {
    ElementManager.cliqueOnElement(String.format("//button[normalize-space() = '%s']", s), driver);
  }
}
